﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DziekanatWzim.Models
{
    public class File
    {
        public int Id { get; set; }
        [Display(Name="Ścieżka")]
        public string Path { get; set; }
        [Display(Name="Opis pliku")]
        public string Description { get; set; }
        [Display(Name="Autor")]
        public string Author { get; set; }
    }
}