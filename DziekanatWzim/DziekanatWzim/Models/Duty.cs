﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DziekanatWzim.Models
{
    public class Duty
    {
        public int Id { get; set; }
        [Display(Name="Wykładowca")]
        public string Username { get; set; }
        public string Godziny { get; set; }
        [Display(Name = "Numer pokoju")]
        public string NrPokoju { get; set; }
    }
}