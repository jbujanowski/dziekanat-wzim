﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DziekanatWzim.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required(ErrorMessage="Podaj login")]
        public string Login { get; set; }
        [Required(ErrorMessage="Podaj hasło")]
        [DataType(DataType.Password)]
        [Display(Name="Hasło")]
        public string Password { get; set; }
        public string FullName { get; set; }
        
    }
}