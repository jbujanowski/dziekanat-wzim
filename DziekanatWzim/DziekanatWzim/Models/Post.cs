﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DziekanatWzim.Models
{
    public class Post
    {
        public int Id { get; set; }
        [Display(Name="Tytuł")]
        public string Title { get; set; }
        [Display(Name="Zawartość")]
        public string Content { get; set; }
        [Display(Name="Autor")]
        public string Author { get; set; }
        public string AttachementPath { get; set; }
        [Display(Name="Data utworzenia")]
        public DateTime CreatedDate { get; set; }
        [Display(Name="Data edycji")]
        public DateTime UpdatedDate { get; set; }
    }
}