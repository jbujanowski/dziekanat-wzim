﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DziekanatWzim.Models;

namespace DziekanatWzim.Controllers
{
    public class TestController : Controller
    {
        private DziekanatWzimContext db = new DziekanatWzimContext();

        // GET: Test
        public ActionResult Index()
        {
            return View(db.Testowies.ToList());
        }

        // GET: Test/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testowy testowy = db.Testowies.Find(id);
            
            if (testowy == null)
            {
                return HttpNotFound();
            }
            return View(testowy);
        }

        // GET: Test/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Test/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Stringi,Wartosc,CreatedDate,UpdateDate")] Testowy testowy)
        {
            if (ModelState.IsValid)
            {
                db.Testowies.Add(testowy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(testowy);
        }

        // GET: Test/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testowy testowy = db.Testowies.Find(id);
            if (testowy == null)
            {
                return HttpNotFound();
            }
            return View(testowy);
        }

        // POST: Test/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Stringi,Wartosc,CreatedDate,UpdateDate")] Testowy testowy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(testowy).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(testowy);
        }

        // GET: Test/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testowy testowy = db.Testowies.Find(id);
            if (testowy == null)
            {
                return HttpNotFound();
            }
            return View(testowy);
        }

        // POST: Test/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Testowy testowy = db.Testowies.Find(id);
            db.Testowies.Remove(testowy);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
