﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DziekanatWzim.Models;

namespace DziekanatWzim.Controllers
{
    public class DutyController : Controller
    {
        private DziekanatWzimContext db = new DziekanatWzimContext();

        // GET: Duty
        public ActionResult Index()
        {
            return View(db.Duties.ToList());
        }

        // GET: Duty/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duty duty = db.Duties.Find(id);
            if (duty == null)
            {
                return HttpNotFound();
            }
            return View(duty);
        }

        // GET: Duty/Create
        public ActionResult Create()
        {
            if (Session["LoggedUserId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
           
        }

        // POST: Duty/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Godziny,NrPokoju")] Duty duty)
        {
            
                if (ModelState.IsValid)
                {
                    db.Duties.Add(duty);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            return View(duty);
        }

        // GET: Duty/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["LoggedUserId"] != null)
            {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duty duty = db.Duties.Find(id);
            if (duty == null)
            {
                return HttpNotFound();
            }
            return View(duty);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: Duty/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Godziny,NrPokoju")] Duty duty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(duty).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(duty);
        }

        // GET: Duty/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["LoggedUserId"] != null)
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Duty duty = db.Duties.Find(id);
                if (duty == null)
                {
                    return HttpNotFound();
                }
                return View(duty);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: Duty/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Duty duty = db.Duties.Find(id);
            db.Duties.Remove(duty);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
//