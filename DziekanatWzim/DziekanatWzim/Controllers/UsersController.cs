﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DziekanatWzim.Models;

namespace DziekanatWzim.Controllers
{
    public class UsersController : Controller
    {
        private DziekanatWzimContext db = new DziekanatWzimContext();

        // GET: Users
        //public ActionResult Index()
        //{
        //    return View(db.Users.ToList());
        //}

        public ActionResult Login() 
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User u) 
        {
            if (ModelState.IsValid)
            {
                var v = db.Users.Where(m => m.Login.Equals(u.Login)&&m.Password.Equals(u.Password)).FirstOrDefault();
                if (v!=null)
                {
                    Session["LoggedUserID"] = v.Id.ToString();
                    Session["LoggedUserFullName"] = v.FullName.ToString();
                    return RedirectToAction("AfterLogin");
                }
            
            }
            return View(u);
        }
        public ActionResult AfterLogin() 
        {
            if (Session["LoggedUserID"]!=null)
            {
                ViewBag.IsLogged = true;
                return View();
            }
            else
            {
                return RedirectToAction("Index","Home");
            }
        }
        public ActionResult LogOut() 
        {
            if (Session["LoggedUserID"]!=null)
            {
                Session["LoggedUserID"] = null;
                ViewBag.IsLogged = false;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
