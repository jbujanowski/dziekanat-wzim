namespace DziekanatWzim.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Duties", "NrPokoju", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Duties", "NrPokoju");
        }
    }
}
