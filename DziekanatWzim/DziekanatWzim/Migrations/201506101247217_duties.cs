namespace DziekanatWzim.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class duties : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Duties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Godziny = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Duties");
        }
    }
}
