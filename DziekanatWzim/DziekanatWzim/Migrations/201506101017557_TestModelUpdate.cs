namespace DziekanatWzim.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestModelUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Testowies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Stringi = c.String(),
                        Wartosc = c.Double(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        lol = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Testowies");
        }
    }
}
